package stepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginStepDefinitions {
	
	@Given("^given statement is executed$")
    public void given_statement_is_executed() throws Throwable {
       System.out.println("Given statement is executed");
    }
 
    @When("^when statemnet is executed$")
    public void when_statemnet_is_executed() throws Throwable {
         System.out.println("When statement is executed");
    }
 
    @Then("^then statement is executed$")
    public void then_statement_is_executed() throws Throwable {
         System.out.println("Then statement is executed");
    }

}
