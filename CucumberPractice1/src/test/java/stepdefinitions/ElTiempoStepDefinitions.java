package stepdefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ElTiempoStepDefinitions {
	
	WebDriver driverET;
	
	@Given("^User is on google search$")
    public void user_is_on_google_search() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LORE\\eclipse-workspace\\Drivers\\chromedriver.exe");
		driverET = new ChromeDriver();
		driverET.get("https://www.google.com//");
		driverET.manage().window().maximize();
		System.out.println("navegador");
    }

    	@When("^User enter in search bar \"([^\"]*)\" keyword$")
    	public void user_enter_in_search_bar_something_keyword(String el_tiempo_bogota) throws Throwable {
    		driverET.findElement(By.name("q")).sendKeys(el_tiempo_bogota);    	
    	}
    
    	@And("^click on search button$")
    	public void click_on_search_button() throws Throwable, InterruptedException {
    		Thread.sleep(3000);
    		driverET.findElement(By.name("btnK")).click();
    	}

    	@Then("^User navigate to the next page$")
    	public void user_navigate_to_the_next_page() throws Throwable {
    		System.out.println(driverET.getTitle());
    	}
    
    	@And("^User click on the first url of the results$")
    	public void user_click_on_the_first_url_of_the_results() throws Throwable {
    		driverET.findElement(By.xpath("//*[@class='LC20lb DKV0Md']")).click();
    	}

    	@Then("^User verify that the page is \"([^\"]*)\"$")
    	public void user_verify_that_the_page_is_something(String URL) throws Throwable {
    		Assert.assertEquals("https://www.eltiempo.com/bogota", driverET.getCurrentUrl());
    	}

    	@And("^User closes el tiempo page$")
    	public void user_closes_el_tiempo_page() throws Throwable, InterruptedException {
    		Thread.sleep(3000);
    		driverET.close();
    	}
}
