package stepdefinitions;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GSearchStepDefinitions {
	
	WebDriver driver;

@Test

	@Given("^User is on google search page$")
    public void user_is_on_google_search_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LORE\\eclipse-workspace\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
        driver.get("https://www.google.com//");
        driver.manage().window().maximize();
    }

	    @When("^User enter \"([^\"]*)\" keyword$")
	    public void user_enter_something_keyword(String selenium) throws Throwable {
	    	driver.findElement(By.name("q")).sendKeys(selenium);
	    }
	    
	    @And("^click on Search button$")
	    public void click_on_search_button() throws Throwable, InterruptedException {
	    	Thread.sleep(3000);
	    	driver.findElement(By.name("btnK")).click();
	    }

	    @Then("^user navigate to the next page$")
	    public void user_navigate_to_the_next_page() throws Throwable {
	    	System.out.println(driver.getTitle());
	    }
	    
	    @And("^select the \"([^\"]*)\" page in results$")
	    public void select_the_something_page_in_results(String seleniumpage) throws Throwable {
	    	driver.findElement(By.xpath("//*[@class='LC20lb DKV0Md']")).click();
	    }

	    @Then("^User closes the window$")
	    public void user_closes_the_window() throws Throwable, InterruptedException{
	    	Thread.sleep(3000);
	        driver.close();
	    }

	    	 
@Test

		@Given("^User is on google search page window$")
		public void user_is_on_google_search_page_window() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LORE\\eclipse-workspace\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.google.com//");
		driver.manage().window().maximize();
		}

		@When("^User enter \"([^\"]*)\" keyword on the searching bar$")
		public void user_enter_something_keyword_on_the_searching_bar(String selenium) throws Throwable {
			driver.findElement(By.name("q")).sendKeys(selenium);;
		}

		@And("^click on I am feeling lucky button$")
		public void click_on_i_am_feeling_lucky_button() throws Throwable, InterruptedException {
			Thread.sleep(3000);
			driver.findElement(By.name("btnI")).click();
		}

		@Then("^User can see the page \"([^\"]*)\"$")
		public void user_can_see_the_page_something(String Title) throws Throwable {
			Assert.assertEquals("SeleniumHQ Browser Automation", driver.getTitle());
			//Assert.assertEquals("SeleniumHQ Browser Automation", driver.getCurrentUrl());
		}
		
		@And("^User closes the page$")
	    public void user_closes_the_page() throws Throwable, InterruptedException{
	    	Thread.sleep(3000);
	        driver.close();
	    }
	    
}
