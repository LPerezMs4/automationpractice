Feature: El Tiempo Page

@tag1
  Scenario Outline: User wants to test the access to 'el tiempo.com' trough google search.
  Given User is on google search
	When User enter in search bar "el tiempo bogota" keyword
	And click on search button
	Then User navigate to the next page
	And User click on the first url of the results
	Then User verify that the page is "https://eltiempo.com/Bogota"
	And User closes el tiempo page
