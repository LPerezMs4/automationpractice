Feature: Google Search	

@Tag1
  Scenario Outline: The User wants to test the search functionality of google search.
	Given User is on google search page
	When User enter "selenium" keyword
	And click on Search button
	Then user navigate to the next page
	And select the "selenium.com" page in results
	Then User closes the window
	
@Tag2
  Scenario Outline: The User wants to test the i am feeling lucky functionality of google search.
	Given User is on google search page window
	When User enter "selenium" keyword on the searching bar
	And click on I am feeling lucky button
	Then User can see the page "https://www.selenium.dev/"
	And User closes the page
	