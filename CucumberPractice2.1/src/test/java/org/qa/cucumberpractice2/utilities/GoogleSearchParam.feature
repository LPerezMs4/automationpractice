Feature: Google Search parametrization Test

Scenario Outline: The User wants to test the search functionality of google search
	Given User is on google search page
	When User enter "<word>" keyword
	And click on Search button
	Then user navigate to the next page

Examples: 
    | word     |
    | Selenium |
    | testNG   |
    | Junit    |
