package org.qa.cucumberpractice2.runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\test\\java\\org\\qa\\cucumberpractice2\\utilities",
                 glue = "org.qa.cucumberpractice2.stepDefinitions")

public class RunnerParamTest {

}