package org.qa.cucumberpractice2.stepDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GSearchParamStepDefinitions {

	WebDriver driver;

	@Given("^User is on google search page$")
	public void user_is_on_google_search_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\LORE\\eclipse-workspace\\Drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.google.com//");
		driver.manage().window().maximize();
	}

	@When("^User enter \"([^\"]*)\" keyword$")
	public void user_enter_something_keyword(String word) throws Throwable {
		driver.findElement(By.name("q")).sendKeys(word);
	}

	@And("^click on Search button$")
	public void click_on_search_button() throws Throwable {
		Thread.sleep(3000);
		driver.findElement(By.name("btnK")).click();
	}

	@Then("^user navigate to the next page$")
	public void user_navigate_to_the_next_page() throws Throwable {
		System.out.println(driver.getTitle());
	}

}
